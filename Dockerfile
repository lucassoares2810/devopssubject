FROM python:3-alpine3.15
WORKDIR /app
COPY . /app
RUN pip install --upgrade pip
RUN python3 -V
RUN apk update  # Atualiza o sistema de pacotes (para Alpine Linux)
RUN apk add py3-virtualenv  # Instala o pacote py3-virtualenv (para Alpine Linux)
RUN python3 -m venv venv
RUN source venv/bin/activate
RUN python -m pip install ngrok
RUN pip install -r requirements.txt
EXPOSE 5000
ENTRYPOINT python3 run.py


